
"""
    _isnan(x)

Default `isnan` expects `Numbers` as argument. This version returns false for
eltype's that are not `Number` and checks for `NaN` for all inputs that are of
type `Number`
"""
_isnan(x) = typeof(x) <: Number ? isnan(x) : false



"""
    _bitmatrix(tbl, [:missing, :nan, "string", vals])

Takes a table as an argument and returns a `Matrix{Bool}` where `true` represents
missing values. By default the function only looks for `missing` values. For expanding
the criteria for what represents missing values `missing_values` can be passed a Vector
of values that should be considered missing.

    - :missing => for `missing` values (required)
    - :nan => for `NaN` values
    - string/values => for everything else

"""
function _bitmatrix(tbl, missing_values = [:missing])  
    # initialize a boolean matrix to store state of missing values in table
    nrow, ncol = size(tbl)
    res = zeros(Union{Missing,Bool}, (nrow, ncol))
    for c in 1:ncol
        for mv in missing_values
            if mv == :missing
                res[:, c] = res[:, c] .|| ismissing.(tbl[:, c])
            elseif mv == :nan
                res[:, c] = res[:, c] .|| _isnan.(tbl[:, c])
            else
                res[:, c] = res[:, c] .|| map(x->x==mv, tbl[:, c])
            end
        end
    end
    return res
end


"""
    _luminance(c)
    _luminance(r, g, b)

Calculated the luminance of a color (perceived brightness) given RGB as input. Values over
0.7 for background colors are good candidates to have a darker font.

"""
_luminance(r,g,b) = 0.2126*r + 0.7152*g + 0.0722*b
_luminance(c::ColorTypes.RGB{Float64}) = _luminance(c.r, c.g, c.b)


"""
    _maxcols(resolution::Tuple)

Returns maximum number of columns after which per cell annotation should be disabled as it 
would result in unreadable plot. 
"""
_maxcols(resolution::Tuple) = round(Int, resolution[1]/32)


"""
    _maxbins(resolution::Tuple)

Returns maximum number of bins after which per cell annotation should be disabled as it 
would result in unreadable plot. 
"""
_maxbins(resolution::Tuple) = round(Int, resolution[2]/32)


"""
    _getcolor(cmap::ColorScheme, val::Float)

Get RGB color from colorscheme mapping val (0-1) in the range of colorscheme
"""
function _getcolor(cmap::ColorScheme, val::Float64)
    @assert (val <= 1) && (val >= 0)
    
    rng = length(cmap)
    
    col = ceil(Int, val * rng)
    col = (col == 0) ? 1 : col # Handle 0 by using the first color 
    
    return cmap[col]
end



"""
    numcols(tbl)

Return number of columns in a Tables.jl compatible table
"""
numcols(tbl) = columns(tbl) |> length


"""
    numrows(tbl)

Return number of rows in a Tables.jl compatible table
"""
function numrows(tbl)
    ncol = numcols(tbl)
    return ncol > 0 ? length( getcolumn(tbl, 1) ) : 0
end