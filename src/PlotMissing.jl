module PlotMissing

import Tables: columns, getcolumn, columnnames
using CairoMakie
using Statistics
using ColorTypes
using ColorSchemes




include("utils.jl")

export missing_heatmap,
       missing_barplot



struct MissingTable
    nrows::Int
    ncols::Int
    nbins::Int
    
    binsize::Int
    lastbinsize::Int
    
    colnames::Vector
    data::Matrix{Int}
end



function MissingTable(tbl, nbins=20; missing_values=[:missing])
    nrow = numrows(tbl)
    ncol = numcols(tbl)
    colnames = columnnames(tbl)
    
    data = zeros(Int, (nbins, ncol))
    
    binsize = div(nrow, nbins)
    lastbinsize = mod(nrow, nbins) == 0 ? binsize : binsize + mod(nrow, nbins)
    
    for i in 1:nbins # For each group (row)     
        st = (i-1) * binsize + 1
        en = i != nbins ? i * binsize : i * binsize + (lastbinsize-binsize) 
        
        # For each column in the subset of table
        for (idx, c) in enumerate(colnames) # For each column of group
            val = 0
            for mv in missing_values
                if mv == :missing
                    val = val + (ismissing.(getcolumn(tbl, c)[st:en]) |> sum)
                elseif mv == :nan
                    val = val + _isnan.(getcolumn(tbl, c)[st:en]) |> sum
                else
                    val = val + map(x->x==mv, getcolumn(tbl, c)[st:en]) |> sum
                end
            end
            data[i, idx] = val
        end
    end
    
    MissingTable(nrow, ncol, nbins, binsize, lastbinsize, colnames, data)   
end


function ratio(m::MissingTable)
    res = similar(m.data, Float64) .= 0
    
    for i in 1:m.nbins
        bs = i == m.nbins ? m.binsize : m.lastbinsize
        res[i,:] = m.data[i,:] ./ bs
    end
    res
end



"""
Decent Palette's to try
- :viridis
- :tab10
- :tab20, :tab20b, :tab20c
- :grayC
- :Accent_8

"""
function missing_heatmap(tbl; 
                        nbins=nothing, 
                        missing_values=[:missing], 
                        resolution=(1200,600), 
                        colormap=:grayC, 
                        title="Missing Heatmap", 
                        xlabel="",
                        ylabel="",
                        annotation=true,
                        colorbar=true 
                        )

    # Calculate binsize based on resolution
    nbins = (nbins === nothing) ? _maxbins(resolution) : nbins

    mt = PlotMissing.MissingTable(tbl, nbins; missing_values=missing_values)

    fig = Figure(resolution=resolution, colormap=colormap)
    ax = Axis(fig[1,1], 
                title = title,
                ylabel = ylabel,
                xlabel = xlabel,
                xticks = (1:length(mt.colnames), string.(mt.colnames)),
                xticklabelrotation = 1.5708,
                xticklabelalign=(:right, :center),
                xticklabelpad=3
        )

    if colorbar == true
        Colorbar(fig[1,2], limits = (0, 1), colormap = colormap)
    end

    hmap = mt |> ratio
    heatmap!(ax, hmap')

    # Annotations 
    annotate_maxcols = _maxcols(resolution)
    annotate_maxbins = _maxbins(resolution)

    if (nbins <= annotate_maxbins) && (mt.ncols <= annotate_maxcols) && (annotation == true)
        for x in 1:size(hmap)[1]
            for y in 1:size(hmap)[2]
                val = hmap[x,y]
                txt = string(round(val, digits=2))

                color = _getcolor(getfield(ColorSchemes, colormap), val)
                lum = _luminance(color)

                txtcolor = lum > 0.6 ? :black : :white

                text!(txt, position=Point(y,x), align = (:center, :center), color = txtcolor, textsize=10)
            end
        end
    end

    fig
end


function missing_barplot(tbl;
                        missing_values=[:missing],
                        resolution=(1200,600), 
                        colormap=:grayC, 
                        title="Missing Barplot", 
                        xlabel="",
                        ylabel="",
                        annotation=true,
                        label_offset=0,
                        label_size=10
    )

    mt=PlotMissing.MissingTable(tbl)

    fig = Figure(resolution=resolution, colormap=colormap)
    ax = Axis(fig[1,1], 
                title = title,
                ylabel = ylabel,
                xlabel = xlabel,
                xticks = (1:length(mt.colnames), string.(mt.colnames)),
                xticklabelrotation = 1.5708,
                xticklabelalign=(:right, :center), limits=(0,nothing, 0, nothing),
                xticklabelpad=3
        )
        
    val=sum(mt.data, dims=1)[end,:]

    if annotation == true
        bar_labels = round.(val ./ mt.nrows, digits=2)
        #bar_labels = val
    else
        bar_labels = nothing
    end
    

    barplot!(ax, val; color=val, bar_labels=bar_labels, label_size=label_size, label_offset=label_offset)
    fig
end


end # module PlotMissing
