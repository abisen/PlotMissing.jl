# PlotMissing.jl 

Library to visualize missing values in Tabular Data. Currently only works with [DataFrames.jl](https://dataframes.juliadata.org/stable/), but eventually it should work with any datasaet that supports `Tables.jl` interface. 

## Example

```julia
missing_heatmap(df; 
                colormap=:Accent_8, 
                resolution=(1000,600), 
                annotation=true, 
                missing_values=[:missing], 
                colorbar=true
                )
```

![Image](docs/img/missing_01.png)


```julia
missing_heatmap(df; 
                colormap=:grayC, 
                resolution=(1000,600), 
                annotation=true, 
                missing_values=[:missing, "CRD", :nan], 
                colorbar=true
                )
```

![Image](docs/img/missing_02.png)


```julia
missing_heatmap(df; 
                colormap=:inferno, 
                resolution=(400,600), 
                annotation=true, 
                missing_values=[:missing], 
                colorbar=false)
```

![Image](docs/img/missing_03.png)