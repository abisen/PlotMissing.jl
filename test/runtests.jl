using PlotMissing
using ColorSchemes
using DataFrames
import PlotMissing: _getcolor, _luminance, _isnan, _bitmatrix

using Test 

@testset "basic tests" begin
    viridis_colormap = getfield(ColorSchemes, :viridis)
    rgb = _getcolor(viridis_colormap, 0.1)

    @test typeof(rgb) == ColorSchemes.ColorTypes.RGB{Float64}
    @test (rgb.r, rgb.g, rgb.b) == (0.282623, 0.140926, 0.457517)

    @test _luminance(rgb) ≈ 0.1939086524
    @test _luminance(rgb.r, rgb.g, rgb.b) ≈ 0.1939086524

    lst = [1, "a", nothing, missing, NaN]
    @test _isnan.(lst) == [false, false, false, false, true]
end


@testset "BitMatrix" begin
    df = DataFrame(
                t1=[1, 2, 3, 4, NaN], 
                t2=["A", "B", NaN, missing, 23] 
                )
    @test _bitmatrix(df) == [0 0; 0 0; 0 0; 0 1; 0 0]
    @test _bitmatrix(df, [:missing, :nan]) == [0 0; 0 0; 0 1; 0 1; 1 0]
    @test _bitmatrix(df, [:missing, "A"]) == [0 1; 0 0; 0 0; 0 1; 0 0]
end